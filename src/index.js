import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import "bootstrap/dist/css/bootstrap.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import CardPage from "./containers/CharacterCards/CardPage/CardPage";
import CardsPage from "./containers/CharacterCards/CardsPage/CardsPage";
import locationCardPage from "./containers/LocationCards/CardPage/CardPage";
import locationCardsPage from "./containers/LocationCards/CardsPage/CardsPage";

ReactDOM.render(
  <Router>
    <App>
      <Route path="/" exact component={CardsPage} />
      <Route path="/character/:item" component={CardPage} />

      <Route path="/location" exact component={locationCardsPage} />
      <Route path="/location/:item" component={locationCardPage} />
    </App>
  </Router>,
  document.getElementById("root")
);

serviceWorker.unregister();
