import React, { Component } from "react";
import CharacterCards from "../../../components/CharacterCard/CharacterCard";
import SearchComponent from "../../SearchComponent/SearchComponent";
class CardsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rickMorty: [],
      characterCards: []
    };
  }
  getData(name) {
    fetch("https://rickandmortyapi.com/api/character/?name=" + name)
      .then(Response => Response.json())
      .then(data => {
        if (data.results) {
          this.setState({ characterCards: data.results });
        }
      });
  }
  componentDidMount() {
    this.getData("");
  }

  render() {
    let characters = null;
    console.log(this.state.characterCards);
    if (this.state.characterCards.length > 0) {
      characters = this.state.characterCards.map(character => (
        <CharacterCards
          key={character.id}
          {...character}
          showLink={true}
        ></CharacterCards>
      ));
    } else {
      characters = <p>Loading...</p>;
    }

    return (
      <React.Fragment>
        <SearchComponent
          onChange={state => {
            this.getData(state.name);
          }}
        />
        <h4>Characters</h4>
        <br />
        <div className="row">{characters}</div>
        <h1>Rick and Morty!</h1>
      </React.Fragment>
    );
  }
}

export default CardsPage;
