import React, { Component } from "react";
import CharacterCards from "../../../components/CharacterCard/CharacterCard";

class CardPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      character: null
    };
  }
  getCharacterData() {
    fetch(
      `https://rickandmortyapi.com/api/character/${this.props.match.params.item}`
    )
      .then(Response => Response.json())
      .then(data => {
        console.log("data", data);
        if (!data.error) {
          this.setState({ character: data });
        }
      });
  }
  componentDidMount() {
    this.getCharacterData();
  }
  render() {
    let characterCard = null;
    if (this.state.character) {
      characterCard = (
        <CharacterCards
          key={this.state.character.id}
          {...this.state.character}
        ></CharacterCards>
      );
    } else {
      characterCard = <p>Loading...</p>;
    }

    return <React.Fragment>{characterCard}</React.Fragment>;
  }
}

export default CardPage;
