import React, { Component } from "react";

class SearchComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: ""
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleonClick = this.handleonClick.bind(this);
  }

  handleNameChange(e) {
    const input = e.target.value;
    this.setState({ name: input });
  }
  handleonClick(e) {
    if (this.props.onChange) {
      this.props.onChange(this.state);
    }
  }

  render() {
    return (
      <div>
        <div className="form-group">
          <label>Name of Character</label>
          <input
            type="text"
            onChange={this.handleNameChange}
            className="form-control"
            placeholder="Search for a Rick &
            Morty characater"
          />
          <br></br>
          <div>
            <button type="button" onClick={this.handleonClick}>
              Search
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default SearchComponent;
