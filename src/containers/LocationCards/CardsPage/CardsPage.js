import React, { Component } from "react";

import LocatopnCards from "../../../components/LocationCard/locationrCard";
import SearchComponent from "../../SearchComponent/SearchComponent";
class CardsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rickMorty: [],
      locationCards: []
    };
  }
  getData(name) {
    fetch("https://rickandmortyapi.com/api/location/?name=" + name)
      .then(Response => Response.json())
      .then(data => {
        if (data.results) {
          this.setState({ locationCards: data.results });
        }
      });
  }
  componentDidMount() {
    this.getData("");
  }

  render() {
    let locations = null;
    console.log(this.state.locationCards);
    if (this.state.locationCards.length > 0) {
      locations = this.state.locationCards.map(location => (
        <LocatopnCards
          key={location.id}
          {...location}
          showLink={true}
        ></LocatopnCards>
      ));
    } else {
      locations = <p>Loading...</p>;
    }

    return (
      <React.Fragment>
        <SearchComponent
          onChange={state => {
            this.getData(state.name);
          }}
        />
        <h4>location</h4>
        <br />
        <div className="row">{locations}</div>
        <h1>Rick and Morty!</h1>
      </React.Fragment>
    );
  }
}

export default CardsPage;
