import React, { Component } from "react";

import LocarionCards from "../../../components/LocationCard/locationrCard";

class CardPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      location: null
    };
  }
  getCharacterData() {
    fetch(
      `https://rickandmortyapi.com/api/location/${this.props.match.params.item}`
    )
      .then(Response => Response.json())
      .then(data => {
        console.log("data", data);
        if (!data.error) {
          this.setState({ location: data });
        }
      });
  }
  componentDidMount() {
    this.getCharacterData();
  }
  render() {
    let locationCard = null;
    if (this.state.location) {
      locationCard = (
        <LocarionCards
          key={this.state.location.id}
          {...this.state.location}
        ></LocarionCards>
      );
    } else {
      locationCard = <p>Loading...</p>;
    }

    return <React.Fragment>{locationCard}</React.Fragment>;
  }
}

export default CardPage;
