import React from "react";
import { Link } from "react-router-dom";

var linkButton = null;

const locationrCard = props => {
  linkButton = (
    <Link
      to={{ pathname: "/location/" + props.id }}
      className="btn btn-primary"
    >
      View
    </Link>
  );
  console.log("test", props);
  return (
    <div className="col-xs-12 col-sm-6 col-md-4">
      <div className="card LocationCard">
        <div className="card-body">
          <h5 className="card-title">{props.name}</h5>
          <b>dimension:</b>
          {props.dimension}
          <br />
          <b>Type:</b>
          {props.type}
          <br />
          {props.showLink ? linkButton : null}
          <br />
        </div>
      </div>
    </div>
  );
};
export default locationrCard;
