import React from "react";
import "./App.css";
import { NavLink } from "react-router-dom";

class App extends React.Component {
  render() {
    return (
      <div>
        <header>
          <img
            src="https://i2.wp.com/www.pixelcrumb.com/wp-content/uploads/2016/10/RICK-AND-MORTY-BANNER.jpg?fit=1920%2C720"
            style={{ margin: "0", display: "block", width: "100%" }}
            alt="Rick and Morty"
          />
        </header>
        <NavLink
          exact
          to={{ pathname: "/" }}
          className="nav-link btn btn-light"
        >
          Home
        </NavLink>
        <NavLink
          exact
          to={{ pathname: "/location" }}
          className="nav-link btn btn-light"
        >
          Location
        </NavLink>
        <div className="characterContainer">{this.props.children}</div>
      </div>
    );
  }
}

export default App;
